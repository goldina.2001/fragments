package com.goldina.fragmentnav

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import androidx.core.view.isVisible
import androidx.navigation.findNavController
import androidx.navigation.ui.AppBarConfiguration
import androidx.navigation.ui.setupActionBarWithNavController
import androidx.navigation.ui.setupWithNavController
import com.goldina.fragmentnav.databinding.ActivityMainBinding
import com.google.android.material.bottomnavigation.BottomNavigationItemView
import com.google.android.material.bottomnavigation.BottomNavigationView

class MainActivity : AppCompatActivity() {
    private lateinit var binding: ActivityMainBinding
    override fun onCreate(savedInstanceState: Bundle?){
        super.onCreate(savedInstanceState)
        binding = ActivityMainBinding.inflate(layoutInflater)
        setContentView(binding.root)
        binding.bottomNav.isVisible=false
        binding.bottomNav.setOnItemSelectedListener{
            when(it.itemId){
                R.id.home -> supportFragmentManager.beginTransaction()
                    .replace(R.id.fragmentContainerView,HomeFragment())
                    .commit()
                R.id.web->{
                    supportFragmentManager.beginTransaction()
                        .replace(R.id.fragmentContainerView,WebFragment())
                        .commit()
                }
            }
            true
        }
    }
}